import socket, select, re


NICK   = "BenrobBot"
USER   = "benrob0329"
PREFIX = '-'
SERVER = "irc.rizon.net"
PORT   = 6667

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
irc_in  = ""
irc_out = \
	"NICK " + NICK + '\r\n' + \
	"USER " + USER + " 0 * python3\r\n"
exit = 0

msgre  = re.compile("(?:@(?P<tags>.*?) )?(?::(?P<source>.*?) )?(?P<command>.*?) (?:(?P<destination>.*?) )?(?P<parameters>.*?)\r\n")
nickre = re.compile("(?P<nick>.*?)!.*$")
comre  = re.compile(":(?P<command>.*?) (?P<param>.*)")


sock.connect((SERVER, PORT))
print("Connected")


while not exit:
	irc_in += sock.recv(1).decode()
	print(irc_in[-1], end='')
	
	message = msgre.match(irc_in)
	if message != None:
		irc_in = msgre.sub(irc_in, "")

		if message.group("source") != None:
			nick = nickre.match(message.group("source"))
			if nick != None:
				nick = nick.group("nick")
			else:
				nick = message.group("source")

		print(message.groups())
		if message.group("command") == "PING":
			irc_out += "PONG " + message.group("parameters") + "\r\n";
		elif message.group("command") == "PRIVMSG":
			command = comre.match(message.group("parameters"))
			if command != None:
				if command.group("command") == PREFIX + "kick":
					irc_out += "PRIVMSG " + message.group("destination") + " :\x01ACTION kicks " + command.group("param") + "\x01\r\n"
				elif command.group("command") == PREFIX + "join":
					irc_out += "JOIN " + command.group("param") + "\r\n"


	for outmsg in irc_out.splitlines(True):
		print(outmsg.encode())
		MSGLEN = len(outmsg)
		totalsent = 0
		while totalsent < MSGLEN:
			sent = sock.send(outmsg[totalsent:].encode())
			if sent == 0:
				raise RuntimeError("socket connection broken")
			totalsent = totalsent + sent
	irc_out = ""
	


sock.shutdown(0)
sock.close()
